module Test.Main where
import Data.Serialize.Get (Get, getVarInt, getWord16be, runGet)
import Prelude (Unit, bind, discard, pure, show, ($), (*>), (<>))
import Control.Alternative ((<|>))
import Data.Either (fromRight)
import Data.List.Lazy (List, some)
import Data.Memory.Encoding.Base32 as B32
import Effect (Effect)
import Effect.Class.Console (logShow)
import Effect.Console (log)
import IPFS.Multiaddr as M
import IPFS.Multiaddr.IP.IPv4 as I4
import IPFS.Multiaddr.IP.IPv6 as IP6
import IPFS.Multiaddr.Protocol as P
import Node.Path (FilePath)
import Partial.Unsafe (unsafePartial)
import Text.Parsing.Parser (Parser, runParser)
import Text.Parsing.Parser.String (char, string)

main :: Effect Unit
main = do
  log "You should add some tests."
  testCodecAddr


testBase32::Effect Unit
testBase32 = do 
  let buf =  B32.decodeBS "2w2qd15ym7wk650rprtuh4vk26eqcqym"
  log $ show $ buf
  log $ show $ B32.encodeBS buf 


testUnixPath::Effect Unit
testUnixPath = do
   let (p::FilePath) = "/va/ww/a.txt"
   log $ p <> "!!"



parseHdr::String -> Parser String String
parseHdr s = parseSep *> (string s)
   

aaa::Parser String String
aaa = (parseHdr "a") <|> (parseHdr "/wss")

parseSep::Parser String (List Char)
parseSep = some $ char '/'


testParse::Effect Unit
testParse  = do
  log $ show $ runParser "123.123.123.123" I4.parse
  let ip6 = runParser "023:0DB8:0:0023:0008:0800:200C:417A" IP6.parse
  log $ show ip6
  log $ show $ runParser "QmPZ3ZfLNSiiQXj75Ft5Qs2qnPCt1Csp1nZBUppfY997Yb" P.parseMultiHash

testProtocol::Effect Unit
testProtocol = do
  log $ show $  M.fromString "/asdtcp/66/ip4/0.0.0.0"


testEncode::Effect Unit
testEncode = do
  let i4 = unsafePartial $ fromRight $ runParser "/tcp/666" P.parse
  let ip6 = unsafePartial $ fromRight $ runParser "023:0DB8:0:0023:0008:0800:200C:417A" IP6.parse
  logShow $ P.encode i4
{-
testCodecIP6::Effect Unit
testCodecIP6 = do
  let i6 = unsafePartial $ fromRight $ runParser "023:0DB8:0:0023:0008:0800:200C:417A" IP6.parse
  logShow i6
  logShow $ IP6.encode i6
  logShow $ IP6.decode $ IP6.encode i6

testCodecPort::Effect Unit
testCodecPort = do
  let port = unsafePartial $ fromJust $ IP.toPort 4399
  logShow port
  let bs = IP.encodePort port
  logShow bs
  logShow $ IP.decodePort bs
-}

testCodecProtocl::Effect Unit
testCodecProtocl = do
  let i4 = unsafePartial $ fromRight $ runParser "/sctp/555" P.parse
  let bytes = P.encode i4
  logShow bytes
  logShow $ runGet P.decode bytes


testCodecAddr::Effect Unit
testCodecAddr = do
  let addr = unsafePartial $ fromRight $ M.fromString "/ip4/123.231.21.123/tcp/111/ipfs/QmPZ3ZfLNSiiQXj75Ft5Qs2qnPCt1Csp1nZBUppfY997Yb"
  logShow addr
  let codeArr  = M.encode addr
  logShow $ codeArr
  logShow $  M.fromBytes codeArr
  logShow $ M.nodeAddress addr




readTTT::Get String
readTTT = do
  w0 <- getVarInt
  w1 <- getWord16be
  pure $ show w0 <> show w1