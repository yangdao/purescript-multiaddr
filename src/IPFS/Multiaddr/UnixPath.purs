module IPFS.Multiaddr.UnixPath(UnixPath(..),encode,decode) where
import Prelude

import Data.ByteString (Encoding(..))
import Data.ByteString as BS
import Data.Maybe (fromJust)
import Node.Path (FilePath)
import Partial.Unsafe (unsafePartial)


newtype UnixPath = UnixPath { path :: FilePath }

instance showUnixPath :: Show UnixPath where
   show (UnixPath u) =  u.path

encode::UnixPath -> BS.ByteString
encode (UnixPath p)= unsafePartial $ fromJust $ BS.fromString  p.path Binary

decode::BS.ByteString -> UnixPath
decode bs = UnixPath {path:BS.toString bs UTF8} 