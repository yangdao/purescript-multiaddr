module IPFS.Multiaddr.Onion where

import Data.ByteString (ByteString)
import Data.Memory.Encoding.Base32 (encodeBS)
import Data.String (toLower)
import IPFS.Multiaddr.IP (Port)
import Prelude

data Onion = Onion {onionHash::ByteString , onionPort::Port}

derive instance eqOnion :: Eq Onion

instance showOnion :: Show Onion where
  show (Onion o)  = toLower $ (encodeBS o.onionHash) <> ":" <> (show o.onionPort)