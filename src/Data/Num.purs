module Data.Num(class Num,negate,abs,fromBigInt) where

import Data.BigInt as BI
import Data.Int as I
import Data.Maybe (fromMaybe)
import Data.Ord as O
import Data.Ring as R
import Prelude (identity,($), (<<<))

class Num a where
  negate::a -> a
  abs::a -> a
  fromBigInt::BI.BigInt -> a

instance numInt :: Num Int where
  negate        = R.negate
  abs           = O.abs
  fromBigInt bi = fromMaybe 0 $ (I.fromNumber <<< BI.toNumber) bi

instance numNumber :: Num Number where
   negate        = R.negate
   abs           = O.abs
   fromBigInt    = BI.toNumber

instance numBigInt :: Num BI.BigInt where
   negate        = R.negate
   abs           = O.abs
   fromBigInt    = identity