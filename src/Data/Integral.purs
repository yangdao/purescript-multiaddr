module Data.Integral(class Integral, toBigInt
, fromIntegral) where
import Prelude

import Data.BigInt as BI
import Data.Maybe (fromMaybe)
import Data.Num (class Num,fromBigInt)

class (Num a) <= Integral a where
  toBigInt :: a -> BI.BigInt


fromIntegral :: forall a b. Integral a => Integral b => a -> b
fromIntegral = fromBigInt <<< toBigInt

instance integralInt :: Integral Int where
 toBigInt i = BI.fromInt i

instance integralNumber :: Integral Number where
  toBigInt n = fromMaybe (BI.fromInt 0) (BI.fromNumber n)

instance integralBigInt :: Integral BI.BigInt where
 toBigInt = identity