module Data.Memory.Encoding.Base32(encode,decode,encodeBS,decodeBS) where

import Prelude

import Data.ByteString (ByteString, unsafeFreeze, unsafeThaw)
import Effect.Unsafe (unsafePerformEffect)
import Node.Buffer (Buffer, fromString)
import Node.Encoding (Encoding(..))


foreign import encode :: Buffer -> String

foreign import decode_ :: String -> String

decode::String -> Buffer
decode str = unsafePerformEffect $ fromString (decode_ str) Binary

encodeBS::ByteString -> String
encodeBS bs = encode $ unsafeThaw bs

decodeBS::String -> ByteString
decodeBS str = unsafeFreeze $ decode str