"use strict";

var base32 = require("base32");

exports.encode = function(x) {
    return base32.encode(x)
}

exports.decode_ = function(b) {
    return base32.decode(b)
}